import * as express from  'express'
import * as cors from 'cors'

const app = express()
const port = 3001

var corsOptions = {
  origin: 'http://localhost:3000',
  optionsSuccessStatus: 200
}
app.use(cors(corsOptions))

app.get('/', (req, res)=> {
  // TODO: Remove this log
  console.log('REQUEST!')
  res.send('Hi!')
})

app.listen(port, ()=> {
  console.log(`App listening at port ${port}`)
})
